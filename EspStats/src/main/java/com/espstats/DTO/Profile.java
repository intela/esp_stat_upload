package com.espstats.DTO;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "profile", schema = "offerbonanza")
public class Profile {
	@Id
	@Column(name = "profile_id", nullable = false)
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer profile_id;
	
	@Column(name = "affiliate_id", nullable = true)
	private Integer affiliate_id;
	
	@Column(name = "parent_list_id", nullable = true)
	private Integer parent_list_id;
	
	@Column(name = "profile_name", nullable = true)
	private String profile_name;
	
	@Column(name = "insert_date", nullable = true)
	private Date insert_date;
	
	@Column(name = "list_qty", nullable = true)
	private Integer list_qty;
	
	@Column(name = "client_id", nullable = true)
	private Integer client_id;
	
	@Column(name = "status", nullable = true)
	private Integer status;
	
	@Column(name = "unsub_image_id", nullable = true)
	private Integer unsub_image_id;
	
	@Column(name = "profile_title", nullable = true)
	private String profile_title;
	
	@Column(name = "profile_country_id", nullable = true)
	private Integer profile_country_id;
	
	@Column(name = "profile_mailer_id", nullable = true)
	private Integer profile_mailer_id;
	
	@Column(name = "esp", nullable = true)
	private Boolean esp;
	
	@Column(name = "lists_list_id", nullable = true)
	private Integer lists_list_id;
	
	@Column(name = "language_lkup_id", nullable = true)
	private Integer language_lkup_id;
	
	@Column(name = "domain_type", nullable = true)
	private String domain_type;
	
	@Column(name = "distribution_id", nullable = false)
	private Integer distribution_id;
	
	@Column(name = "circuit_id", nullable = true)
	private Integer circuit_id;

	public Integer getProfile_id() {
		return profile_id;
	}

	public void setProfile_id(Integer profile_id) {
		this.profile_id = profile_id;
	}

	public Integer getAffiliate_id() {
		return affiliate_id;
	}

	public void setAffiliate_id(Integer affiliate_id) {
		this.affiliate_id = affiliate_id;
	}

	public Integer getParent_list_id() {
		return parent_list_id;
	}

	public void setParent_list_id(Integer parent_list_id) {
		this.parent_list_id = parent_list_id;
	}

	public String getProfile_name() {
		return profile_name;
	}

	public void setProfile_name(String profile_name) {
		this.profile_name = profile_name;
	}

	public Date getInsert_date() {
		return insert_date;
	}

	public void setInsert_date(Date insert_date) {
		this.insert_date = insert_date;
	}

	public Integer getList_qty() {
		return list_qty;
	}

	public void setList_qty(Integer list_qty) {
		this.list_qty = list_qty;
	}

	public Integer getClient_id() {
		return client_id;
	}

	public void setClient_id(Integer client_id) {
		this.client_id = client_id;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getUnsub_image_id() {
		return unsub_image_id;
	}

	public void setUnsub_image_id(Integer unsub_image_id) {
		this.unsub_image_id = unsub_image_id;
	}

	public String getProfile_title() {
		return profile_title;
	}

	public void setProfile_title(String profile_title) {
		this.profile_title = profile_title;
	}

	public Integer getProfile_country_id() {
		return profile_country_id;
	}

	public void setProfile_country_id(Integer profile_country_id) {
		this.profile_country_id = profile_country_id;
	}

	public Integer getProfile_mailer_id() {
		return profile_mailer_id;
	}

	public void setProfile_mailer_id(Integer profile_mailer_id) {
		this.profile_mailer_id = profile_mailer_id;
	}

	public Boolean getEsp() {
		return esp;
	}

	public void setEsp(Boolean esp) {
		this.esp = esp;
	}

	public Integer getLists_list_id() {
		return lists_list_id;
	}

	public void setLists_list_id(Integer lists_list_id) {
		this.lists_list_id = lists_list_id;
	}

	public Integer getLanguage_lkup_id() {
		return language_lkup_id;
	}

	public void setLanguage_lkup_id(Integer language_lkup_id) {
		this.language_lkup_id = language_lkup_id;
	}

	public String getDomain_type() {
		return domain_type;
	}

	public void setDomain_type(String domain_type) {
		this.domain_type = domain_type;
	}

	public Integer getDistribution_id() {
		return distribution_id;
	}

	public void setDistribution_id(Integer distribution_id) {
		this.distribution_id = distribution_id;
	}

	public Integer getCircuit_id() {
		return circuit_id;
	}

	public void setCircuit_id(Integer circuit_id) {
		this.circuit_id = circuit_id;
	}
	
	
}
