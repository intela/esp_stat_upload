package com.espstats.DTO;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "esp_report", schema = "offerbonanza")
public class EspReport {
	
	@Id
	@Column(name = "esp_report_id", nullable = false)
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer esp_report_id;
	
	@Column(name = "campaign_id", nullable = false)
	private Integer campaign_id;
	
	@Column(name = "profile_id", nullable = true)
	private Integer profile_id;
	
	@Column(name = "offer_id", nullable = false)
	private Integer offer_id;
	
	@Column(name = "sent_qty", nullable = true)
	private Integer sent_qty;
	
	@Column(name = "delivered_qty", nullable = true)
	private Integer delivered_qty;
	
	@Column(name = "click_qty", nullable = true)
	private Integer click_qty;
	
	@Column(name = "open_qty", nullable = true)
	private Integer open_qty;
	
	@Column(name = "hard_bounce_qty", nullable = true)
	private Integer hard_bounce_qty;
	
	@Column(name = "soft_bounce_qty", nullable = true)
	private Integer soft_bounce_qty;
	
	@Column(name = "complainers", nullable = true)
	private Integer complainers;
	
	@Column(name = "unsubscribes", nullable = true)
	private Integer unsubscribes;
	
	@Column(name = "create_date", nullable = true)
	private Date create_date;

	public Integer getEsp_report_id() {
		return esp_report_id;
	}

	public void setEsp_report_id(Integer esp_report_id) {
		this.esp_report_id = esp_report_id;
	}

	public Integer getCampaign_id() {
		return campaign_id;
	}

	public void setCampaign_id(Integer campaign_id) {
		this.campaign_id = campaign_id;
	}

	public Integer getProfile_id() {
		return profile_id;
	}

	public void setProfile_id(Integer profile_id) {
		this.profile_id = profile_id;
	}

	public Integer getOffer_id() {
		return offer_id;
	}

	public void setOffer_id(Integer offer_id) {
		this.offer_id = offer_id;
	}

	public Integer getSent_qty() {
		return sent_qty;
	}

	public void setSent_qty(Integer sent_qty) {
		this.sent_qty = sent_qty;
	}

	public Integer getDelivered_qty() {
		return delivered_qty;
	}

	public void setDelivered_qty(Integer delivered_qty) {
		this.delivered_qty = delivered_qty;
	}

	public Integer getClick_qty() {
		return click_qty;
	}

	public void setClick_qty(Integer click_qty) {
		this.click_qty = click_qty;
	}

	public Integer getOpen_qty() {
		return open_qty;
	}

	public void setOpen_qty(Integer open_qty) {
		this.open_qty = open_qty;
	}

	public Integer getHard_bounce_qty() {
		return hard_bounce_qty;
	}

	public void setHard_bounce_qty(Integer hard_bounce_qty) {
		this.hard_bounce_qty = hard_bounce_qty;
	}

	public Integer getSoft_bounce_qty() {
		return soft_bounce_qty;
	}

	public void setSoft_bounce_qty(Integer soft_bounce_qty) {
		this.soft_bounce_qty = soft_bounce_qty;
	}

	public Integer getComplainers() {
		return complainers;
	}

	public void setComplainers(Integer complainers) {
		this.complainers = complainers;
	}

	public Integer getUnsubscribes() {
		return unsubscribes;
	}

	public void setUnsubscribes(Integer unsubscribes) {
		this.unsubscribes = unsubscribes;
	}

	public Date getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}
	
}
