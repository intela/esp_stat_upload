package com.espstats.DTO;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "campaign", schema = "offerbonanza")
public class Campaign {
	@Id
	@Column(name = "campaign_id", nullable = false)
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer campaign_id;
	
	@Column(name = "io_id", nullable = false)
	private Integer io_id;
	
	@Column(name = "io_offer_id", nullable = false)
	private Integer io_offer_id;
	
	@Column(name = "list_id", nullable = true)
	private Integer list_id;
	
	@Column(name = "offer_id", nullable = false)
	private Integer offer_id;
	
	@Column(name = "circuit_id", nullable = true)
	private Integer circuit_id;
	
	@Column(name = "sub_offer_id", nullable = true)
	private Integer sub_offer_id;
	
	@Column(name = "parent_campaign_id", nullable = true)
	private Integer parent_campaign_id;
	
	@Column(name = "campaign_date", nullable = true)
	private Date campaign_date;
	
	@Column(name = "name", nullable = true)
	private String name;
	
	@Column(name = "user_id", nullable = true)
	private Integer user_id;
	
	@Column(name = "mail_server", nullable = true)
	private Integer mail_server;
	
	@Column(name = "mtas", nullable = true)
	private String mtas;
	
	@Column(name = "notes", nullable = true)
	private String notes;
	
	@Column(name = "campaign_status_id", nullable = true)
	private Integer campaign_status_id;
	
	@Column(name = "suppression_list", nullable = true)
	private Boolean suppression_list;
	
	@Column(name = "test_campaign", nullable = true)
	private Boolean test_campaign;
	
	@Column(name = "html_name", nullable = true)
	private String html_name;
	
	@Column(name = "delivered", nullable = true)
	private Integer delivered;
	
	@Column(name = "hard_bounce", nullable = true)
	private Integer hard_bounce;
	
	@Column(name = "soft_bounce", nullable = true)
	private Integer soft_bounce;
	
	@Column(name = "sent", nullable = true)
	private Integer sent;
	
	@Column(name = "modified_date", nullable = true)
	private Date modified_date;
	
	@Column(name = "smtp_host_name", nullable = true)
	private String smtp_host_name;
	
	@Column(name = "smtp_host_userid", nullable = true)
	private String smtp_host_userid;
	
	@Column(name = "smtp_host_password", nullable = true)
	private String smtp_host_password;
	
	@Column(name = "campaign_execution_constraint", nullable = true)
	private String campaign_execution_constraint;
	
	@Column(name = "campaign_suppression_dir", nullable = true)
	private String campaign_suppression_dir;
	
	@Column(name = "campaign_ignore_global_suppression_list", nullable = true)
	private String campaign_ignore_global_suppression_list;
	
	@Column(name = "campaign_invalid_domains_file", nullable = true)
	private String campaign_invalid_domains_file;
	
	@Column(name = "email_from_address", nullable = true)
	private String email_from_address;
	
	@Column(name = "email_from_hosts", nullable = true)
	private String email_from_hosts;
	
	@Column(name = "email_from_name", nullable = true)
	private String email_from_name;
	
	@Column(name = "email_message_subject_delimiter", nullable = true)
	private String email_message_subject_delimiter;
	
	@Column(name = "email_message_subject", nullable = true)
	private String email_message_subject;
	
	@Column(name = "email_message_subject_id", nullable = true)
	private Integer email_message_subject_id;
	
	@Column(name = "template_type", nullable = true)
	private String template_type;
	
	@Column(name = "template_content_type", nullable = true)
	private String template_content_type;
	
	@Column(name = "template_url", nullable = true)
	private String template_url;
	
	@Column(name = "messages_per_thread", nullable = true)
	private String messages_per_thread;
	
	@Column(name = "number_of_threads", nullable = true)
	private String number_of_threads;
	
	@Column(name = "data_file_name", nullable = true)
	private String data_file_name;
	
	@Column(name = "data_file_type", nullable = true)
	private String data_file_type;
	
	@Column(name = "data_record_delimiter", nullable = true)
	private String data_record_delimiter;
	
	@Column(name = "data_record_descriptor", nullable = true)
	private String data_record_descriptor;
	
	@Column(name = "default_immed_responder", nullable = true)
	private Integer default_immed_responder;
	
	@Column(name = "campaign_type_id", nullable = true)
	private Integer campaign_type_id;
	
	@Column(name = "new_creative", nullable = true)
	private Boolean new_creative;
	
	@Column(name = "template_content_characterset", nullable = true)
	private String template_content_characterset;
	
	@Column(name = "big_creative", nullable = true)
	private Boolean big_creative;
	
	@Column(name = "creative_size", nullable = true)
	private Integer creative_size;
	
	@Column(name = "list_type_id", nullable = true)
	private Integer list_type_id;
	
	@Column(name = "data_start", nullable = true)
	private Integer data_start;
	
	@Column(name = "data_end", nullable = true)
	private Integer data_end;
	
	@Column(name = "category_type_id", nullable = true)
	private Integer category_type_id;
	
	@Column(name = "encode_html", nullable = true)
	private Boolean encode_html;
	
	@Column(name = "encode_hex", nullable = true)
	private Boolean encode_hex;
	
	@Column(name = "offer_creative_id", nullable = true)
	private Integer offer_creative_id;
	
	@Column(name = "create_date", nullable = true)
	private Date create_date;
	
	@Column(name = "seed_file", nullable = true)
	private String seed_file;
	
	@Column(name = "seed_type", nullable = true)
	private String seed_type;
	
	@Column(name = "seed_freq", nullable = true)
	private Integer seed_freq;
	
	@Column(name = "xheaders", nullable = true)
	private String xheaders;
	
	@Column(name = "burstsize", nullable = true)
	private Integer burstsize;
	
	@Column(name = "bursttime", nullable = true)
	private Integer bursttime;
	
	@Column(name = "comment", nullable = true)
	private Boolean comment;
	
	@Column(name = "international", nullable = true)
	private Boolean international;
	
	@Column(name = "listunsubscribe", nullable = true)
	private Boolean listunsubscribe;
	
	@Column(name = "comment_id", nullable = true)
	private Integer comment_id;
	
	@Column(name = "sr_blacklist", nullable = true)
	private Boolean sr_blacklist;
	
	@Column(name = "host_domain_id", nullable = true)
	private Integer host_domain_id;
	
	@Column(name = "currency_type_lkup_id", nullable = false)
	private Integer currency_type_lkup_id;
	
	@Column(name = "list_country_type_lkup_id", nullable = false)
	private Integer list_country_type_lkup_id;
	
	@Column(name = "distribution_id", nullable = false)
	private Integer distribution_id;
	
	@Column(name = "headerfooter_template_id", nullable = true)
	private Integer headerfooter_template_id;
	
	@Column(name = "use_mailing_ip", nullable = true)
	private Integer use_mailing_ip;
	
	@Column(name = "external_profile", nullable = true)
	private Boolean external_profile;

	public Integer getCampaign_id() {
		return campaign_id;
	}

	public void setCampaign_id(Integer campaign_id) {
		this.campaign_id = campaign_id;
	}

	public Integer getIo_id() {
		return io_id;
	}

	public void setIo_id(Integer io_id) {
		this.io_id = io_id;
	}

	public Integer getIo_offer_id() {
		return io_offer_id;
	}

	public void setIo_offer_id(Integer io_offer_id) {
		this.io_offer_id = io_offer_id;
	}

	public Integer getList_id() {
		return list_id;
	}

	public void setList_id(Integer list_id) {
		this.list_id = list_id;
	}

	public Integer getOffer_id() {
		return offer_id;
	}

	public void setOffer_id(Integer offer_id) {
		this.offer_id = offer_id;
	}

	public Integer getCircuit_id() {
		return circuit_id;
	}

	public void setCircuit_id(Integer circuit_id) {
		this.circuit_id = circuit_id;
	}

	public Integer getSub_offer_id() {
		return sub_offer_id;
	}

	public void setSub_offer_id(Integer sub_offer_id) {
		this.sub_offer_id = sub_offer_id;
	}

	public Integer getParent_campaign_id() {
		return parent_campaign_id;
	}

	public void setParent_campaign_id(Integer parent_campaign_id) {
		this.parent_campaign_id = parent_campaign_id;
	}

	public Date getCampaign_date() {
		return campaign_date;
	}

	public void setCampaign_date(Date campaign_date) {
		this.campaign_date = campaign_date;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getUser_id() {
		return user_id;
	}

	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}

	public Integer getMail_server() {
		return mail_server;
	}

	public void setMail_server(Integer mail_server) {
		this.mail_server = mail_server;
	}

	public String getMtas() {
		return mtas;
	}

	public void setMtas(String mtas) {
		this.mtas = mtas;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Integer getCampaign_status_id() {
		return campaign_status_id;
	}

	public void setCampaign_status_id(Integer campaign_status_id) {
		this.campaign_status_id = campaign_status_id;
	}

	public Boolean getSuppression_list() {
		return suppression_list;
	}

	public void setSuppression_list(Boolean suppression_list) {
		this.suppression_list = suppression_list;
	}

	public Boolean getTest_campaign() {
		return test_campaign;
	}

	public void setTest_campaign(Boolean test_campaign) {
		this.test_campaign = test_campaign;
	}

	public String getHtml_name() {
		return html_name;
	}

	public void setHtml_name(String html_name) {
		this.html_name = html_name;
	}

	public Integer getDelivered() {
		return delivered;
	}

	public void setDelivered(Integer delivered) {
		this.delivered = delivered;
	}

	public Integer getHard_bounce() {
		return hard_bounce;
	}

	public void setHard_bounce(Integer hard_bounce) {
		this.hard_bounce = hard_bounce;
	}

	public Integer getSoft_bounce() {
		return soft_bounce;
	}

	public void setSoft_bounce(Integer soft_bounce) {
		this.soft_bounce = soft_bounce;
	}

	public Integer getSent() {
		return sent;
	}

	public void setSent(Integer sent) {
		this.sent = sent;
	}

	public Date getModified_date() {
		return modified_date;
	}

	public void setModified_date(Date modified_date) {
		this.modified_date = modified_date;
	}

	public String getSmtp_host_name() {
		return smtp_host_name;
	}

	public void setSmtp_host_name(String smtp_host_name) {
		this.smtp_host_name = smtp_host_name;
	}

	public String getSmtp_host_userid() {
		return smtp_host_userid;
	}

	public void setSmtp_host_userid(String smtp_host_userid) {
		this.smtp_host_userid = smtp_host_userid;
	}

	public String getSmtp_host_password() {
		return smtp_host_password;
	}

	public void setSmtp_host_password(String smtp_host_password) {
		this.smtp_host_password = smtp_host_password;
	}

	public String getCampaign_execution_constraint() {
		return campaign_execution_constraint;
	}

	public void setCampaign_execution_constraint(
			String campaign_execution_constraint) {
		this.campaign_execution_constraint = campaign_execution_constraint;
	}

	public String getCampaign_suppression_dir() {
		return campaign_suppression_dir;
	}

	public void setCampaign_suppression_dir(String campaign_suppression_dir) {
		this.campaign_suppression_dir = campaign_suppression_dir;
	}

	public String getCampaign_ignore_global_suppression_list() {
		return campaign_ignore_global_suppression_list;
	}

	public void setCampaign_ignore_global_suppression_list(
			String campaign_ignore_global_suppression_list) {
		this.campaign_ignore_global_suppression_list = campaign_ignore_global_suppression_list;
	}

	public String getCampaign_invalid_domains_file() {
		return campaign_invalid_domains_file;
	}

	public void setCampaign_invalid_domains_file(
			String campaign_invalid_domains_file) {
		this.campaign_invalid_domains_file = campaign_invalid_domains_file;
	}

	public String getEmail_from_address() {
		return email_from_address;
	}

	public void setEmail_from_address(String email_from_address) {
		this.email_from_address = email_from_address;
	}

	public String getEmail_from_hosts() {
		return email_from_hosts;
	}

	public void setEmail_from_hosts(String email_from_hosts) {
		this.email_from_hosts = email_from_hosts;
	}

	public String getEmail_from_name() {
		return email_from_name;
	}

	public void setEmail_from_name(String email_from_name) {
		this.email_from_name = email_from_name;
	}

	public String getEmail_message_subject_delimiter() {
		return email_message_subject_delimiter;
	}

	public void setEmail_message_subject_delimiter(
			String email_message_subject_delimiter) {
		this.email_message_subject_delimiter = email_message_subject_delimiter;
	}

	public String getEmail_message_subject() {
		return email_message_subject;
	}

	public void setEmail_message_subject(String email_message_subject) {
		this.email_message_subject = email_message_subject;
	}

	public Integer getEmail_message_subject_id() {
		return email_message_subject_id;
	}

	public void setEmail_message_subject_id(Integer email_message_subject_id) {
		this.email_message_subject_id = email_message_subject_id;
	}

	public String getTemplate_type() {
		return template_type;
	}

	public void setTemplate_type(String template_type) {
		this.template_type = template_type;
	}

	public String getTemplate_content_type() {
		return template_content_type;
	}

	public void setTemplate_content_type(String template_content_type) {
		this.template_content_type = template_content_type;
	}

	public String getTemplate_url() {
		return template_url;
	}

	public void setTemplate_url(String template_url) {
		this.template_url = template_url;
	}

	public String getMessages_per_thread() {
		return messages_per_thread;
	}

	public void setMessages_per_thread(String messages_per_thread) {
		this.messages_per_thread = messages_per_thread;
	}

	public String getNumber_of_threads() {
		return number_of_threads;
	}

	public void setNumber_of_threads(String number_of_threads) {
		this.number_of_threads = number_of_threads;
	}

	public String getData_file_name() {
		return data_file_name;
	}

	public void setData_file_name(String data_file_name) {
		this.data_file_name = data_file_name;
	}

	public String getData_file_type() {
		return data_file_type;
	}

	public void setData_file_type(String data_file_type) {
		this.data_file_type = data_file_type;
	}

	public String getData_record_delimiter() {
		return data_record_delimiter;
	}

	public void setData_record_delimiter(String data_record_delimiter) {
		this.data_record_delimiter = data_record_delimiter;
	}

	public String getData_record_descriptor() {
		return data_record_descriptor;
	}

	public void setData_record_descriptor(String data_record_descriptor) {
		this.data_record_descriptor = data_record_descriptor;
	}

	public Integer getDefault_immed_responder() {
		return default_immed_responder;
	}

	public void setDefault_immed_responder(Integer default_immed_responder) {
		this.default_immed_responder = default_immed_responder;
	}

	public Integer getCampaign_type_id() {
		return campaign_type_id;
	}

	public void setCampaign_type_id(Integer campaign_type_id) {
		this.campaign_type_id = campaign_type_id;
	}

	public Boolean getNew_creative() {
		return new_creative;
	}

	public void setNew_creative(Boolean new_creative) {
		this.new_creative = new_creative;
	}

	public String getTemplate_content_characterset() {
		return template_content_characterset;
	}

	public void setTemplate_content_characterset(
			String template_content_characterset) {
		this.template_content_characterset = template_content_characterset;
	}

	public Boolean getBig_creative() {
		return big_creative;
	}

	public void setBig_creative(Boolean big_creative) {
		this.big_creative = big_creative;
	}

	public Integer getCreative_size() {
		return creative_size;
	}

	public void setCreative_size(Integer creative_size) {
		this.creative_size = creative_size;
	}

	public Integer getList_type_id() {
		return list_type_id;
	}

	public void setList_type_id(Integer list_type_id) {
		this.list_type_id = list_type_id;
	}

	public Integer getData_start() {
		return data_start;
	}

	public void setData_start(Integer data_start) {
		this.data_start = data_start;
	}

	public Integer getData_end() {
		return data_end;
	}

	public void setData_end(Integer data_end) {
		this.data_end = data_end;
	}

	public Integer getCategory_type_id() {
		return category_type_id;
	}

	public void setCategory_type_id(Integer category_type_id) {
		this.category_type_id = category_type_id;
	}

	public Boolean getEncode_html() {
		return encode_html;
	}

	public void setEncode_html(Boolean encode_html) {
		this.encode_html = encode_html;
	}

	public Boolean getEncode_hex() {
		return encode_hex;
	}

	public void setEncode_hex(Boolean encode_hex) {
		this.encode_hex = encode_hex;
	}

	public Integer getOffer_creative_id() {
		return offer_creative_id;
	}

	public void setOffer_creative_id(Integer offer_creative_id) {
		this.offer_creative_id = offer_creative_id;
	}

	public Date getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}

	public String getSeed_file() {
		return seed_file;
	}

	public void setSeed_file(String seed_file) {
		this.seed_file = seed_file;
	}

	public String getSeed_type() {
		return seed_type;
	}

	public void setSeed_type(String seed_type) {
		this.seed_type = seed_type;
	}

	public Integer getSeed_freq() {
		return seed_freq;
	}

	public void setSeed_freq(Integer seed_freq) {
		this.seed_freq = seed_freq;
	}

	public String getXheaders() {
		return xheaders;
	}

	public void setXheaders(String xheaders) {
		this.xheaders = xheaders;
	}

	public Integer getBurstsize() {
		return burstsize;
	}

	public void setBurstsize(Integer burstsize) {
		this.burstsize = burstsize;
	}

	public Integer getBursttime() {
		return bursttime;
	}

	public void setBursttime(Integer bursttime) {
		this.bursttime = bursttime;
	}

	public Boolean getComment() {
		return comment;
	}

	public void setComment(Boolean comment) {
		this.comment = comment;
	}

	public Boolean getInternational() {
		return international;
	}

	public void setInternational(Boolean international) {
		this.international = international;
	}

	public Boolean getListunsubscribe() {
		return listunsubscribe;
	}

	public void setListunsubscribe(Boolean listunsubscribe) {
		this.listunsubscribe = listunsubscribe;
	}

	public Integer getComment_id() {
		return comment_id;
	}

	public void setComment_id(Integer comment_id) {
		this.comment_id = comment_id;
	}

	public Boolean getSr_blacklist() {
		return sr_blacklist;
	}

	public void setSr_blacklist(Boolean sr_blacklist) {
		this.sr_blacklist = sr_blacklist;
	}

	public Integer getHost_domain_id() {
		return host_domain_id;
	}

	public void setHost_domain_id(Integer host_domain_id) {
		this.host_domain_id = host_domain_id;
	}

	public Integer getCurrency_type_lkup_id() {
		return currency_type_lkup_id;
	}

	public void setCurrency_type_lkup_id(Integer currency_type_lkup_id) {
		this.currency_type_lkup_id = currency_type_lkup_id;
	}

	public Integer getList_country_type_lkup_id() {
		return list_country_type_lkup_id;
	}

	public void setList_country_type_lkup_id(Integer list_country_type_lkup_id) {
		this.list_country_type_lkup_id = list_country_type_lkup_id;
	}

	public Integer getDistribution_id() {
		return distribution_id;
	}

	public void setDistribution_id(Integer distribution_id) {
		this.distribution_id = distribution_id;
	}

	public Integer getHeaderfooter_template_id() {
		return headerfooter_template_id;
	}

	public void setHeaderfooter_template_id(Integer headerfooter_template_id) {
		this.headerfooter_template_id = headerfooter_template_id;
	}

	public Integer getUse_mailing_ip() {
		return use_mailing_ip;
	}

	public void setUse_mailing_ip(Integer use_mailing_ip) {
		this.use_mailing_ip = use_mailing_ip;
	}

	public Boolean getExternal_profile() {
		return external_profile;
	}

	public void setExternal_profile(Boolean external_profile) {
		this.external_profile = external_profile;
	}
}
