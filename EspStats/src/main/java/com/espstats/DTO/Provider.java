package com.espstats.DTO;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "provider", schema = "offerbonanza")
public class Provider {
	@Id
	@Column(name = "provider_id", nullable = false)
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer provider_id;
	
	@Column(name = "provider_name", nullable = false)
	private String provider_name;
	
	@Column(name = "symbol", nullable = false)
	private String symbol;
	
	@Column(name = "contact_name", nullable = true)
	private String contact_name;
	
	@Column(name = "contact_phone", nullable = true)
	private String contact_phone;
	
	@Column(name = "contact_email", nullable = true)
	private String contact_email;
	
	@Column(name = "contact_aim", nullable = true)
	private String contact_aim;
	
	@Column(name = "create_date", nullable = true)
	private Date create_date;
	
	@Column(name = "distribution_id", nullable = false)
	private Integer distribution_id;
	
	@Column(name = "is_active", nullable = true)
	private boolean is_active;
	
	@Column(name = "report_delimiter", nullable = true)
	private Character report_delimiter;

	public int getProvider_id() {
		return provider_id;
	}

	public void setProvider_id(int provider_id) {
		this.provider_id = provider_id;
	}

	public String getProvider_name() {
		return provider_name;
	}

	public void setProvider_name(String provider_name) {
		this.provider_name = provider_name;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getContact_name() {
		return contact_name;
	}

	public void setContact_name(String contact_name) {
		this.contact_name = contact_name;
	}

	public String getContact_phone() {
		return contact_phone;
	}

	public void setContact_phone(String contact_phone) {
		this.contact_phone = contact_phone;
	}

	public String getContact_email() {
		return contact_email;
	}

	public void setContact_email(String contact_email) {
		this.contact_email = contact_email;
	}

	public String getContact_aim() {
		return contact_aim;
	}

	public void setContact_aim(String contact_aim) {
		this.contact_aim = contact_aim;
	}

	public Date getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}

	public int getDistribution_id() {
		return distribution_id;
	}

	public void setDistribution_id(int distribution_id) {
		this.distribution_id = distribution_id;
	}

	public boolean isIs_active() {
		return is_active;
	}

	public void setIs_active(boolean is_active) {
		this.is_active = is_active;
	}

	public char getReport_delimiter() {
		return report_delimiter;
	}

	public void setReport_delimiter(char report_delimiter) {
		this.report_delimiter = report_delimiter;
	}
	
}
