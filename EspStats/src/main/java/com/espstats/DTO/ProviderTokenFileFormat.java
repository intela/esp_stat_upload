package com.espstats.DTO;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "provider_token_file_format", schema = "offerbonanza")
public class ProviderTokenFileFormat {
	
	@Id
	@Column(name = "provider_token_file_format_id", nullable = false)
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer provider_token_file_format_id;
	
	@Column(name = "provider_id", nullable = false)
	private Integer provider_id;
	
	@Column(name = "display_label", nullable = true)
	private String display_label;
	
	@Column(name = "mapped_column", nullable = true)
	private String mapped_column;
	
	@Column(name = "created_by", nullable = true)
	private Integer created_by;
	
	@Column(name = "create_date", nullable = true)
	private Date create_date;
	
	@Column(name = "updated_by", nullable = true)
	private Integer updated_by;
	
	@Column(name = "changed_date", nullable = true)
	private Date changed_date;

	public Integer getProvider_token_file_format_id() {
		return provider_token_file_format_id;
	}

	public void setProvider_token_file_format_id(
			Integer provider_token_file_format_id) {
		this.provider_token_file_format_id = provider_token_file_format_id;
	}

	public Integer getProvider_id() {
		return provider_id;
	}

	public void setProvider_id(Integer provider_id) {
		this.provider_id = provider_id;
	}

	public String getDisplay_label() {
		return display_label;
	}

	public void setDisplay_label(String display_label) {
		this.display_label = display_label;
	}

	public String getMapped_column() {
		return mapped_column;
	}

	public void setMapped_column(String mapped_column) {
		this.mapped_column = mapped_column;
	}

	public Integer getCreated_by() {
		return created_by;
	}

	public void setCreated_by(Integer created_by) {
		this.created_by = created_by;
	}

	public Date getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}

	public Integer getUpdated_by() {
		return updated_by;
	}

	public void setUpdated_by(Integer updated_by) {
		this.updated_by = updated_by;
	}

	public Date getChanged_date() {
		return changed_date;
	}

	public void setChanged_date(Date changed_date) {
		this.changed_date = changed_date;
	}
	
}
