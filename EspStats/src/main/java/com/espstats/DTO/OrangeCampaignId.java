package com.espstats.DTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by wonryu on 9/10/15.
 */
@Entity
@Table(name = "orange_campaign_id", schema = "offerbonanza")
public class OrangeCampaignId {
    @Id
    @Column(name = "orange_campaign_id", nullable = false)
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer orangeCampaignId;

    @Column(name = "campaign_id", nullable = false)
    private Integer campaignId;

    public Integer getOrangeCampaignId() {
        return orangeCampaignId;
    }

    public void setOrangeCampaignId(Integer orangeCampaignId) {
        this.orangeCampaignId = orangeCampaignId;
    }

    public Integer getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Integer campaignId) {
        this.campaignId = campaignId;
    }
}
