package com.espstats.DTO;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "circuit", schema = "offerbonanza")
public class Circuit {
	@Id
	@Column(name = "circuit_id", nullable = false)
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer circuit_id;
	
	@Column(name = "mta_id", nullable = true)
	private Integer mta_id;
	
	@Column(name = "circuit_name", nullable = false)
	private String circuit_name;
	
	@Column(name = "ip", nullable = true)
	private String ip;
	
	@Column(name = "date_added", nullable = true)
	private Date date_added;
	
	@Column(name = "status", nullable = false)
	private Integer status;
	
	@Column(name = "type", nullable = true)
	private Integer type;
	
	@Column(name = "unsub_image_id", nullable = true)
	private Integer unsub_image_id;
	
	@Column(name = "list_type_priority", nullable = false)
	private Integer list_type_priority;
	
	@Column(name = "unsub_priority", nullable = true)
	private Integer unsub_priority;
	
	@Column(name = "hotmail_feedback", nullable = true)
	private Boolean hotmail_feedback;
	
	@Column(name = "updated_date", nullable = true)
	private Date updated_date;
	
	@Column(name = "modified_user", nullable = true)
	private Integer modified_user;
	
	@Column(name = "config_file_name", nullable = true)
	private String config_file_name;
	
	@Column(name = "zone1", nullable = true)
	private String zone1;
	
	@Column(name = "zone2", nullable = true)
	private String zone2;
	
	@Column(name = "zone3", nullable = true)
	private String zone3;
	
	@Column(name = "isp_id", nullable = true)
	private Integer isp_id;
	
	@Column(name = "server_cost", nullable = true)
	private Float server_cost;
	
	@Column(name = "ip_cost", nullable = true)
	private Float ip_cost;
	
	@Column(name = "cap", nullable = true)
	private Integer cap;
	
	@Column(name = "list_country_type_lkup_id", nullable = true)
	private Integer list_country_type_lkup_id;
	
	@Column(name = "distribution_id", nullable = false)
	private Integer distribution_id;
	
	@Column(name = "burst_size", nullable = true)
	private Integer burst_size;
	
	@Column(name = "burst_time_minutes", nullable = true)
	private Integer burst_time_minutes;
	
	@Column(name = "image_hosting", nullable = true)
	private String image_hosting;
	
	@Column(name = "unsub_ftp", nullable = true)
	private String unsub_ftp;
	
	@Column(name = "report_upload_ftp", nullable = true)
	private String report_upload_ftp;

	public Integer getCircuit_id() {
		return circuit_id;
	}

	public void setCircuit_id(Integer circuit_id) {
		this.circuit_id = circuit_id;
	}

	public Integer getMta_id() {
		return mta_id;
	}

	public void setMta_id(Integer mta_id) {
		this.mta_id = mta_id;
	}

	public String getCircuit_name() {
		return circuit_name;
	}

	public void setCircuit_name(String circuit_name) {
		this.circuit_name = circuit_name;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Date getDate_added() {
		return date_added;
	}

	public void setDate_added(Date date_added) {
		this.date_added = date_added;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getUnsub_image_id() {
		return unsub_image_id;
	}

	public void setUnsub_image_id(Integer unsub_image_id) {
		this.unsub_image_id = unsub_image_id;
	}

	public Integer getList_type_priority() {
		return list_type_priority;
	}

	public void setList_type_priority(Integer list_type_priority) {
		this.list_type_priority = list_type_priority;
	}

	public Integer getUnsub_priority() {
		return unsub_priority;
	}

	public void setUnsub_priority(Integer unsub_priority) {
		this.unsub_priority = unsub_priority;
	}

	public Boolean getHotmail_feedback() {
		return hotmail_feedback;
	}

	public void setHotmail_feedback(Boolean hotmail_feedback) {
		this.hotmail_feedback = hotmail_feedback;
	}

	public Date getUpdated_date() {
		return updated_date;
	}

	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}

	public Integer getModified_user() {
		return modified_user;
	}

	public void setModified_user(Integer modified_user) {
		this.modified_user = modified_user;
	}

	public String getConfig_file_name() {
		return config_file_name;
	}

	public void setConfig_file_name(String config_file_name) {
		this.config_file_name = config_file_name;
	}

	public String getZone1() {
		return zone1;
	}

	public void setZone1(String zone1) {
		this.zone1 = zone1;
	}

	public String getZone2() {
		return zone2;
	}

	public void setZone2(String zone2) {
		this.zone2 = zone2;
	}

	public String getZone3() {
		return zone3;
	}

	public void setZone3(String zone3) {
		this.zone3 = zone3;
	}

	public Integer getIsp_id() {
		return isp_id;
	}

	public void setIsp_id(Integer isp_id) {
		this.isp_id = isp_id;
	}

	public Float getServer_cost() {
		return server_cost;
	}

	public void setServer_cost(Float server_cost) {
		this.server_cost = server_cost;
	}

	public Float getIp_cost() {
		return ip_cost;
	}

	public void setIp_cost(Float ip_cost) {
		this.ip_cost = ip_cost;
	}

	public Integer getCap() {
		return cap;
	}

	public void setCap(Integer cap) {
		this.cap = cap;
	}

	public Integer getList_country_type_lkup_id() {
		return list_country_type_lkup_id;
	}

	public void setList_country_type_lkup_id(Integer list_country_type_lkup_id) {
		this.list_country_type_lkup_id = list_country_type_lkup_id;
	}

	public Integer getDistribution_id() {
		return distribution_id;
	}

	public void setDistribution_id(Integer distribution_id) {
		this.distribution_id = distribution_id;
	}

	public Integer getBurst_size() {
		return burst_size;
	}

	public void setBurst_size(Integer burst_size) {
		this.burst_size = burst_size;
	}

	public Integer getBurst_time_minutes() {
		return burst_time_minutes;
	}

	public void setBurst_time_minutes(Integer burst_time_minutes) {
		this.burst_time_minutes = burst_time_minutes;
	}

	public String getImage_hosting() {
		return image_hosting;
	}

	public void setImage_hosting(String image_hosting) {
		this.image_hosting = image_hosting;
	}

	public String getUnsub_ftp() {
		return unsub_ftp;
	}

	public void setUnsub_ftp(String unsub_ftp) {
		this.unsub_ftp = unsub_ftp;
	}

	public String getReport_upload_ftp() {
		return report_upload_ftp;
	}

	public void setReport_upload_ftp(String report_upload_ftp) {
		this.report_upload_ftp = report_upload_ftp;
	}
}
