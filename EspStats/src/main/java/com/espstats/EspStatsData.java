package com.espstats;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import com.espstats.DTO.OrangeCampaignId;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.espstats.DTO.Campaign;
import com.espstats.DTO.Circuit;
import com.espstats.DTO.EspReport;
import com.espstats.DTO.Provider;
import com.espstats.DTO.ProviderTokenFileFormat;
import com.espstats.util.HibernateUtil;

@SuppressWarnings("unchecked")
public class EspStatsData 
{
	public final static Log log = LogFactory.getLog(EspStatsData.class);
	
	private Properties prop = null;
	private ZipOutputStream espStatsDataZip = null;
	private List<String> wrongFile=null;
	private List<String> wrongFileFormat=null;
	Calendar dateTime = null;
	public EspStatsData() {
		try {
			log.info("load properties in the constructor");
			prop=new Properties( );
			prop.load(getClass( ).getClassLoader( ).getResourceAsStream("config.properties"));
			wrongFile=new ArrayList<String>();
			wrongFileFormat = new ArrayList<String>();
			dateTime = Calendar.getInstance( );
		} catch (Exception e) {
			log.warn("Failed to initialize correctly.", e);
			e.printStackTrace();
		}
	}
	
	public static void main( String[] args )
    {    	
    	try {
    		EspStatsData espStatsData = new EspStatsData();
	    	espStatsData.process();
	    	if(espStatsData.espStatsDataZip != null){
	    		espStatsData.espStatsDataZip.close();
	    	}
			espStatsData.sendMail(espStatsData.prop.getProperty("sendingEmail"), espStatsData.prop.getProperty("ccEmails"), espStatsData.prop.getProperty("subject"), espStatsData.wrongFile, espStatsData.wrongFileFormat);
		} catch (IOException e) {
			log.error("main method error", e);
		}
    }
	
    public void process(){
    	String delimiter;
    	Provider provider;
    	List<ProviderTokenFileFormat> providerTokenFileFormats;
    	List<EspReport> espReports = new ArrayList<EspReport>();
    	String[] providerSymbol=new String[4];
    	log.info("Process started");
    	try {
    		List<Circuit> circuits=getAllCircuits();
			log.info("total circuits: " + circuits.size());
    		File inputfolderPath = new File(prop.getProperty("inputFilePath"));
    		if (inputfolderPath.exists() && inputfolderPath.isDirectory()) {
    			File espAccountFolder=null;
    			for(Circuit circuit: circuits){
    				if(circuit.getReport_upload_ftp() != null){
    					providerSymbol=circuit.getReport_upload_ftp().split("/");
//						log.info("Checking " + prop.getProperty("inputFilePath")+"/"+providerSymbol[1]+"/"+providerSymbol[2].replace("_reports", ""));
						log.info("Checking " + circuit.getReport_upload_ftp());
	    				espAccountFolder =new File(circuit.getReport_upload_ftp());
	    				if(espAccountFolder.exists() && espAccountFolder.isDirectory()){
							log.info("Found the esp account folder");
		    				File[] espAccountFiles=espAccountFolder.listFiles();
		    				if (espAccountFiles != null && espAccountFiles.length > 0) {
			    				for (File espAccountFile : espAccountFiles) {
									provider = getProvider(providerSymbol[4]);

									if(provider == null) {
										continue;
									}

									delimiter=getDelimiter(provider.getReport_delimiter());
									providerTokenFileFormats = getProviderFileFormat(provider.getProvider_id());
										if (delimiter!= null && providerTokenFileFormats != null && (FilenameUtils.getExtension(espAccountFile.getName( )).equalsIgnoreCase("txt") || FilenameUtils.getExtension(espAccountFile.getName( )).equalsIgnoreCase("csv"))) {
											if (espReports.size() > 0 ) {
												espReports.clear();
											}
											if(checkDropIdExists(providerTokenFileFormats)){
												espReports = constructEspReports(circuit, espAccountFile, delimiter, providerTokenFileFormats);
											}else{
												espStatsDataZip = new ZipOutputStream(new FileOutputStream(prop.getProperty("outputFiles") + "-invalid-" + providerSymbol[1] + "-" + dateTime.getTimeInMillis( )+".zip"));
												archive(espAccountFile, espStatsDataZip, providerSymbol[2]);
												wrongFile.add(espAccountFile.getAbsolutePath());
												log.error("DropId not exist or wrong file uploaded." + espAccountFile.getAbsolutePath());
											}
											if(espReports.size()>0){
												saveEspReports(espReports);
											}
										}
										if (espReports.size() > 0) {
											espStatsDataZip = new ZipOutputStream(new FileOutputStream(prop.getProperty("outputFiles") + "-" + providerSymbol[1] + "-" + dateTime.getTimeInMillis( )+".zip"));
											archive(espAccountFile, espStatsDataZip, providerSymbol[2]);
										}
									}
		    				}
	    				}
    				}
    			}
			
    		}
    	}catch(Exception e){
    		log.error("Process method error", e);
    	}
    	log.info("Process Ended");
    	
    }
    private boolean checkDropIdExists(List<ProviderTokenFileFormat> providerTokenFileFormats){
    	for(ProviderTokenFileFormat providerTokenFileFormat : providerTokenFileFormats){
    		if(providerTokenFileFormat.getMapped_column().equalsIgnoreCase("Campaign ID") || providerTokenFileFormat.getMapped_column().equalsIgnoreCase("Parsed Campaign ID")){
    			return true;
    		}
    	}
    	return false;
    }
    private void saveEspReports(List<EspReport> espReports){
    	List<EspReport> espReports2;
    	EspReport espReport1;
    	log.info("Saving esp report");
    	for (EspReport espReport : espReports) {
    		try {
    			SessionFactory sessionFactory=HibernateUtil.getSessionFactory( );
        		Session session = sessionFactory.openSession( );
        		Session session1 = sessionFactory.openSession( );
        		espReports2 = session.createCriteria(EspReport.class)
    					.add(Restrictions.eq("campaign_id", espReport.getCampaign_id()))
    					.list();  
        		
        		if(espReports2.size() > 0){
        			espReport1=espReports2.get(0);
        			log.info("Updating the existing esp report with campain id="+espReport1.getCampaign_id());
        			EspReport espReport2= (EspReport)session.load(EspReport.class, espReport1.getEsp_report_id());
	    			if (espReport.getSent_qty() == null) {
	    				espReport2.setSent_qty(0);
	    			}else{
	    				espReport2.setSent_qty(espReport.getSent_qty());
	    			}
	    			if (espReport.getDelivered_qty() == null) {
	    				espReport2.setDelivered_qty(0);
	    			}else{
	    				espReport2.setDelivered_qty(espReport.getDelivered_qty());
	    			}
	    			if (espReport.getClick_qty() == null) {
	    				espReport2.setClick_qty(0);
	    			}else{
	    				espReport2.setClick_qty(espReport.getClick_qty());
	    			}
	    			if (espReport.getOpen_qty() == null) {
	    				espReport2.setOpen_qty(0);    				
	    			}else{
	    				espReport2.setOpen_qty(espReport.getOpen_qty());
	    			}
	    			if (espReport.getHard_bounce_qty() == null) {
	    				espReport2.setHard_bounce_qty(0);
	    			}else{
	    				espReport2.setHard_bounce_qty(espReport.getHard_bounce_qty());
	    			}
					if (espReport.getSoft_bounce_qty() == null) {
						espReport2.setSoft_bounce_qty(0);
					}else{
						espReport2.setSoft_bounce_qty(espReport.getSoft_bounce_qty());
					}
					if (espReport.getComplainers() == null) {
						espReport2.setComplainers(0);
					}else{
						espReport2.setComplainers(espReport.getComplainers());
					}
					if (espReport.getUnsubscribes() == null) {
						espReport2.setUnsubscribes(0);
					}else{
						espReport2.setUnsubscribes(espReport.getUnsubscribes());
					}

					Transaction tx = session1.beginTransaction( );
	        		espReport2.setCreate_date(new Date());
	        		session1.update( espReport2 );
	        		tx.commit( );
        			
        		}else{
        			log.info("Inserting new report with campain id="+espReport.getCampaign_id());
	    			if (espReport.getCampaign_id() == null) {
	    				espReport.setCampaign_id(0);
	    			}
	    			if (espReport.getSent_qty() == null) {
	    				espReport.setSent_qty(0);
	    			}
	    			if (espReport.getDelivered_qty() == null) {
	    				espReport.setDelivered_qty(0);
	    			}
	    			if (espReport.getClick_qty() == null) {
	    				espReport.setClick_qty(0);
	    			}
	    			if (espReport.getOpen_qty() == null) {
	    				espReport.setOpen_qty(0);    				
	    			}
	    			if (espReport.getHard_bounce_qty() == null) {
	    				espReport.setHard_bounce_qty(0);
	    			}
					if (espReport.getSoft_bounce_qty() == null) {
						espReport.setSoft_bounce_qty(0);
					}
					if (espReport.getComplainers() == null) {
						espReport.setComplainers(0);
					}
					if (espReport.getUnsubscribes() == null) {
						espReport.setUnsubscribes(0);
					}

	        		Transaction tx = session1.beginTransaction( );
	        		espReport.setCreate_date(new Date());
	        		session1.save( espReport );
	        		tx.commit( );
        		}
        		session1.close();
        		session.close( );
        	} catch (Exception e) {
        		log.error("Error occurred getting information", e);
        	}
    	}
    }

    private Circuit getCircuitIdByCircuitId(int circuitId){
    	List<Circuit> circuits;
    	log.info("Getting circuit by using circuitId");
    	try {
    		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    		Session session = sessionFactory.openSession();
    		circuits = session.createCriteria(Circuit.class)
    					.add(Restrictions.eq("circuit_id", circuitId))
    					.list();  
    		session.close( );
    		if (circuits.size() > 0) {
    			return circuits.get(0);
    		}
    	} catch(Exception e) {
    		log.warn("Error occurred getting information", e);
    	}
    	return null;
    }
    
    private List<Circuit> getAllCircuits(){
    	List<Circuit> circuits = null;
    	log.info("Getting circuit by using circuitId");
    	try {
    		SessionFactory sessionFactory = HibernateUtil.getSessionFactory( );
    		Session session = sessionFactory.openSession( );
    		circuits = session.createCriteria(Circuit.class)    					
    					.list();  
    		session.close( );    		
    	} catch(Exception e) {
    		log.warn("Error occurred getting information", e);
    	}
    	return circuits;
    }

	private int getCampaignId(int orangeCampaignId) {
		OrangeCampaignId oc;
		log.info("Getting campaign by using campaignId");
		try {
			SessionFactory sessionFactory = HibernateUtil.getSessionFactory( );
			Session session = sessionFactory.openSession( );
			oc = (OrangeCampaignId)session.createCriteria(OrangeCampaignId.class)
					.add(Restrictions.eq("orangeCampaignId", orangeCampaignId)).uniqueResult();
			session.close( );

			if(oc != null) {
				return oc.getCampaignId();
			}
		} catch(Exception e) {
			log.error("Error occurred getting information", e);
		}
		return 0;
	}

	private Campaign getCampaign(int orangeCampaignId) {
		int campaignId = getCampaignId(orangeCampaignId);
		return getCampaignByCampaignId(campaignId);
	}
    
    private Campaign getCampaignByCampaignId(int campaignId){
    	List<Campaign> campaigns;
    	log.info("Getting campaign by using campaignId");
    	try {
    		SessionFactory sessionFactory = HibernateUtil.getSessionFactory( );
    		Session session = sessionFactory.openSession( );
    		campaigns = session.createCriteria(Campaign.class)
    					.add(Restrictions.eq("campaign_id", campaignId))
    					.list();        			
    		session.close( );
    		if (campaigns.size() > 0) {
    			return campaigns.get(0);
    		}
    	} catch(Exception e) {
    		log.error("Error occurred getting information", e);
    	}
    	return null;
    }
    
    
    private List<EspReport> constructEspReports(Circuit circuit, File espAccountFile, String delimiter, List<ProviderTokenFileFormat> providerTokenFileFormats){
    	List<EspReport> espReportsList = new ArrayList<EspReport>();
    	BufferedReader reader;
    	String content;
    	String[] contentData;
    	EspReport espReport;
    	boolean providerIdStatus = true;
    	Campaign campaign;
    	int orangeCampaignId;
		int providerCampaignId;
    	ProviderTokenFileFormat providerTokenFileFormat;
    	try {
    		log.info("Constructing esp reports list");
    		reader = new BufferedReader(new FileReader(espAccountFile));
    		while ((content = reader.readLine( )) != null) {    			
				contentData = content.split(delimiter+"");
				providerTokenFileFormat = providerTokenFileFormats.get(providerTokenFileFormats.size()-1);
				if (contentData.length >= Integer.parseInt(providerTokenFileFormat.getDisplay_label().replaceAll("[^0-9]", ""))) {
					espReport = new EspReport();
					for (ProviderTokenFileFormat providerToken : providerTokenFileFormats) {
						if (providerToken.getMapped_column().equalsIgnoreCase("Campaign ID") || providerToken.getMapped_column().equalsIgnoreCase("Parsed Campaign ID")) {
							if(providerToken.getMapped_column().equalsIgnoreCase("Parsed Campaign ID")){
								orangeCampaignId = parseOrangeCampaignID(contentData[getColumnNumber(providerToken.getDisplay_label())].trim());
							} else {
								orangeCampaignId = Integer.parseInt(contentData[getColumnNumber(providerToken.getDisplay_label())].trim());
							}
							providerCampaignId = getCampaignId(orangeCampaignId);
							espReport.setCampaign_id(providerCampaignId);
							if (verifyProviderId(providerCampaignId, providerToken.getProvider_id())) {
								campaign = getCampaignByCampaignId(providerCampaignId);
								if (campaign.getList_id() != null) {
									espReport.setProfile_id(campaign.getList_id());
								}
								if (campaign.getOffer_id() != null) {
									espReport.setOffer_id(campaign.getOffer_id());
								}
							} else {
								log.info("This file is not related to the current provider. The file is" + espAccountFile.getAbsolutePath());
								providerIdStatus = false;
							}
						}
						else if (providerToken.getMapped_column().equalsIgnoreCase("Sent")) {
							espReport.setSent_qty(Integer.parseInt(contentData[getColumnNumber(providerToken.getDisplay_label())].trim()));
						} else if (providerToken.getMapped_column().equalsIgnoreCase("Delivered")) {
							espReport.setDelivered_qty(Integer.parseInt(contentData[getColumnNumber(providerToken.getDisplay_label())].trim()));
						} else if (providerToken.getMapped_column().equalsIgnoreCase("Opens")) {
							espReport.setOpen_qty(Integer.parseInt(contentData[getColumnNumber(providerToken.getDisplay_label())].trim()));
						} else if (providerToken.getMapped_column().equalsIgnoreCase("Clicks")) {
							espReport.setClick_qty(Integer.parseInt(contentData[getColumnNumber(providerToken.getDisplay_label())].trim()));
						} else if (providerToken.getMapped_column().equalsIgnoreCase("Hard Bounces")) {
							espReport.setHard_bounce_qty(Integer.parseInt(contentData[getColumnNumber(providerToken.getDisplay_label())].trim()));
						} else if (providerToken.getMapped_column().equalsIgnoreCase("Soft Bounces")) {
							espReport.setSoft_bounce_qty(Integer.parseInt(contentData[getColumnNumber(providerToken.getDisplay_label())].trim()));
						} else if (providerToken.getMapped_column().equalsIgnoreCase("FBL Complainers")) {
							espReport.setComplainers(Integer.parseInt(contentData[getColumnNumber(providerToken.getDisplay_label())].trim()));
						} else if (providerToken.getMapped_column().equalsIgnoreCase("Unsubscribes")) {
							espReport.setUnsubscribes(Integer.parseInt(contentData[getColumnNumber(providerToken.getDisplay_label())].trim()));
						}
					}
					if (providerIdStatus && espReport.getProfile_id() != null && espReport.getOffer_id() != null) {					
						espReportsList.add(espReport);
					} else {
						providerIdStatus=true;
						espReportsList.clear();
						wrongFile.add(espAccountFile.getAbsolutePath());
						Provider provider = getProvider(circuit.getIsp_id());
						if(provider != null) {
							espStatsDataZip = new ZipOutputStream(new FileOutputStream(prop.getProperty("outputFiles") + "-invalid-" + provider.getSymbol() + "-" + dateTime.getTimeInMillis( )+".zip"));
							archive(espAccountFile, espStatsDataZip, circuit.getCircuit_name());
							wrongFile.add(espAccountFile.getAbsolutePath());
							log.error("DropId not exist or wrong file uploaded." + espAccountFile.getAbsolutePath());

						}
						log.error("DropId not exist or wrong file uploaded." + espAccountFile.getAbsolutePath());
						break;
					}
				} else {
					espReportsList.clear();
					log.error("File columns are not matched with the tokens count file is " + espAccountFile.getAbsolutePath());
					Provider provider = getProvider(circuit.getIsp_id());
					if(provider != null) {
						espStatsDataZip = new ZipOutputStream(new FileOutputStream(prop.getProperty("outputFiles") + "-invalid-" + provider.getSymbol() + "-" + dateTime.getTimeInMillis( )+".zip"));
						archive(espAccountFile, espStatsDataZip, circuit.getCircuit_name());
						wrongFile.add(espAccountFile.getAbsolutePath());
						log.error("DropId not exist or wrong file uploaded." + espAccountFile.getAbsolutePath());

					}
					wrongFileFormat.add(espAccountFile.getAbsolutePath());
					break;
				}
    		}
    		reader.close();
		} catch (Exception e) {
			espReportsList.clear();
			log.error("Error in construct esp reports method", e);
			Provider provider = getProvider(circuit.getIsp_id());
			if(provider != null) {
				try {
					espStatsDataZip = new ZipOutputStream(new FileOutputStream(prop.getProperty("outputFiles") + "-invalid-" + provider.getSymbol() + "-" + dateTime.getTimeInMillis( )+".zip"));
					archive(espAccountFile, espStatsDataZip, circuit.getCircuit_name());
					wrongFile.add(espAccountFile.getAbsolutePath());
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}
				log.error("DropId not exist or wrong file uploaded." + espAccountFile.getAbsolutePath());

			}
			wrongFileFormat.add(espAccountFile.getAbsolutePath());
		}
    	return espReportsList;
    	
    }

    private boolean verifyProviderId(int campaignId, int providerId){
    	log.info("verifying provider id information");
    	try{
	    	Campaign campain = getCampaignByCampaignId(campaignId);
	    	Circuit circuit = getCircuitIdByCircuitId(campain.getCircuit_id());
	    	if (circuit.getIsp_id() == providerId) {
	    		return true;
	    	}
    	}catch(Exception e){
			log.error("Failed to verify provider Id.", e);
    	}
    	return false;
    }
    
    private int getColumnNumber(String column){
    	int columnValue=-1;
    	try{
    	columnValue=Integer.parseInt(column.replaceAll("[^0-9]", ""))-1;
    	}catch(Exception e){
			log.error("Unable to get column number", e);
    	}
    	return columnValue;
    }
    
    private List<ProviderTokenFileFormat> getProviderFileFormat(int providerId){
    	List<ProviderTokenFileFormat> providerTokenFormat = null;
    	try {
    		log.info("Getting provider file field information");
    		SessionFactory sessionFactory=HibernateUtil.getSessionFactory( );
    			Session session = sessionFactory.openSession( );
    			providerTokenFormat = session.createCriteria(ProviderTokenFileFormat.class)
    						.add(Restrictions.eq("provider_id", providerId))
    						.list();    
    			session.close( );
    		} catch(Exception e) {
    			log.error("Error occurred getting information", e);
    		}
    	return providerTokenFormat;
    }
    
    private String getDelimiter(char delimiter){
    	log.info("Identifying delimiter=" + delimiter);
    	String actualDelimiter=null;
    	if ('s' == delimiter) {
    		actualDelimiter=";";
		} else if ('c' == delimiter) {
			actualDelimiter=",";
		} else if ('p' == delimiter) {
			actualDelimiter="\\|";
		} else if ('t' == delimiter) {
			actualDelimiter="\t";
		}
    	return actualDelimiter;
    }
    
    private Provider getProvider(String symbol){
    	List<Provider> providers;
    	try {
    		log.info("Getting provider information with symbol=" + symbol);
    		SessionFactory sessionFactory=HibernateUtil.getSessionFactory( );
    			Session session = sessionFactory.openSession( );
    			providers = session.createCriteria(Provider.class)
    					.add(Restrictions.eq("symbol", symbol))
						.add(Restrictions.eq("distribution_id", 2))
    					.list();
    			session.close( );
    			return providers.get(0);
    		} catch (Exception e) {
    			log.error("Error occurred getting information symbol=[" + symbol + "]", e);
    		}
    	return null;
    }

	private Provider getProvider(int id){
		List<Provider> providers;
		try {
			log.info("Getting provider information with id=" + id);
			SessionFactory sessionFactory=HibernateUtil.getSessionFactory( );
			Session session = sessionFactory.openSession( );
			providers = session.createCriteria(Provider.class)
					.add(Restrictions.eq("provider_id", id))
					.add(Restrictions.eq("distribution_id", 2))
					.list();
			session.close( );
			return providers.get(0);
		} catch (Exception e) {
			log.error("Error occurred getting information", e);
		}
		return null;
	}

	private void archive(File file, ZipOutputStream zipFileName, String subFolder){
    	try {
    		FileInputStream in;
    		log.info("Archiving the file="+file.getName());
			in = new FileInputStream( file );
			zipFileName.putNextEntry(new ZipEntry( subFolder + "/" + file.getName( ))); 
			byte[] b = new byte[1024];
			int count;
			while ((count = in.read(b)) > 0) {
			    zipFileName.write(b, 0, count);
			}		 
    		in.close();
    		removeActualFiles(file);
    		log.info("Archived the file="+file.getName());
		} catch (Exception e) {
			log.error("Archive file Exception", e);
		}
	}
    
    private int parseOrangeCampaignID(String campaignName){
    	String[] split = campaignName.split("_");
    	int campaignID = 0;
    	if(split[0] != null){
    		campaignID = Integer.parseInt(split[0]);
    	}
    	
		return campaignID;
    }
    
	private void removeActualFiles(File file){
		try {
			log.info("File deletion started" + file.getName());
			if (file.delete()) {
	            log.info("File Deleted Successfully");
	        }
			
		} catch (Exception e){
			log.info("File deletion error", e);
		}
	}
	
	private void sendMail(String to, String cc, String subject, List<String> wrongFiles, List<String> wrongFileFormats){
		try {
			log.info("Sending email to "+to);

			String mailData;
			if(wrongFile.size() > 0 || wrongFileFormat.size() > 0){
				mailData = "<font style='color:blue;size:15px;'>" + prop.getProperty("wrongFilesTitle") + "</font><br>";
				for(String wrongFileName : wrongFiles){
					mailData = mailData + wrongFileName + "<br>";
				}
				mailData = mailData + "<font style='color:blue;size:15px;'>" + prop.getProperty("wrongFileFormatsTitle") + "</font><br>";
				for(String wrongFileFormatName : wrongFileFormats){
					mailData = mailData + wrongFileFormatName + "<br>";
				}
			} else {
				mailData = "ESP Stat uploaded successfully.";
			}

			new com.espstats.util.EmailSending( ).sendEmail(to, cc, subject, mailData);
			log.info("sent email to "+to);
		} catch (Exception e) {
			log.error("Failed to send email", e);
		}
	}
}
