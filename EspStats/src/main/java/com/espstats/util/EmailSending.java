package com.espstats.util;



import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailSending {
	
	public void sendEmail( String to, String cc, String subject, String body ){
	     final Properties prop = new Properties( );
	      try{
	    	  prop.load(getClass( ).getClassLoader( ).getResourceAsStream("config.properties"));
	 	      Properties properties = System.getProperties( );
	 	      properties.put("mail.smtp.auth", "true");
	 	      properties.put("mail.smtp.starttls.enable", "true");
	 	      properties.put("mail.smtp.host", prop.getProperty("host"));
	 	      properties.put("mail.smtp.port", prop.getProperty("port"));

	 	      Session session = Session.getInstance(properties,
	 	    		  new javax.mail.Authenticator() {
	 	    			protected PasswordAuthentication getPasswordAuthentication( ) {
	 	    				return new PasswordAuthentication(prop.getProperty("username"), prop.getProperty("password"));
	 	    			}
	 	    		  });
	    	  
	 	     
	    	  
	         MimeMessage message = new MimeMessage(session);
	         message.setFrom(new InternetAddress(prop.getProperty("from")));
	         message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));
	         //message.setRecipients(Message.RecipientType.CC, cc);
	         message.addRecipients(Message.RecipientType.BCC, InternetAddress.parse(cc));
	         message.setSubject(subject);
	         //message.setText(body);
	         message.setContent(body,"text/html");
	         Transport.send(message);
	         System.out.println("Sent message successfully....");
	      }catch (MessagingException mex) {
	         mex.printStackTrace( );
	      }catch(Exception e){
	    	  e.printStackTrace( );
	      }
	}

}
